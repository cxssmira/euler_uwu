;; Problem 20 | https://projecteuler.net/problem=20
;;---------------------------------------------------
;;  
;;  n! means n × (n − 1) × ... × 3 × 2 × 1
;;  
;;  For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
;;  and the sum of the digits in the number 10! is:
;;  
;;        3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
;;  
;;  Find the sum of the digits in the number 100!

(defun factorial (n)
  (labels ((rec (x)
            (if (< x 1)
              1
              (* x (rec (- x 1))))))
          (rec n)))

(setq max (factorial 100))

(setq result 
  #'(loop :for i :below max
    :collect i))

(print result)
