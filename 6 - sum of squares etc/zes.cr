<<-DOC
	Problem 6	~	https://projecteuler.net/problem=6
	--------------------------------------------------

	The difference between the sum of the squares of the first ten natural numbers
	1^2 + 2^2 + ... + 10^2 = 385
	(1 + 2 + ... + 10)^2 = 55^2 = 3025
	and the square of the sum is: 3025 - 385 = 2640

	Find the difference between the sum of the squares 
	of the first one hundred natural numbers and the square of the sum.
	DOC

def sumsquare(x : Int32)
  i : Int32 = 0
  sum : Int32 = 0
  until i == x
    i += 1
    sum += i
  end
  sum * sum
end

def squaresum(x : Int32)
  i : Int32 = 0
  sum : Int32 = 0
  until i == x
    i += 1
    # ::nodoc:: i ^ 2 + ... where i += 1
    sum += i * i
  end
  sum
end

puts sumsquare(100) - squaresum(100)
