require "bit_array"
require "big"

module Zestien
  VERSION = "0.1.0"
  <<-DOC
	# Problem 16	~	https://projecteuler.net/problem=16
	-------------------------------------------------------
	2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
	What is the sum of the digits of the number 2^1000?

	- function that takes a value x and returns the digits of 2^x

	## ideas without GMP
	- function to take input x and return result of 2^x in string format negating 
	  the size constraint
	- since all these results are base16 (or w/e the cs term for the numbers)
		like 1, 2, 4, 8, 16, 32, 64 etc etc
	- we can divide the result to convert it to the denary representation
	- then sum all the digits & multiply by whatever number it is to get the answer

	## with GMP
	- make a power function
	- iterate through the returned value and add digits recursively
	- izzy pizzy

	## __*conclusion: use GMP*__
	DOC

  class Zestien
    def power(x : Int32)
      (2 &** x)
    end

    def iterate(x, u)
      return 0 if x < 1
      u = power x
      n = x % 10
      f = (x / 10).round
      p f
      n ** u + iterate(f, u)
    end

    def bitmanip(x)
    end

    # fuck this shit mane.
    def fuckit
      sum = 0
      fuckinghell = "10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376"
      fuckinghell.each_char do |i|
        sum += fuckinghell[i].to_i16
      end
      sum
    end
  end

  v = Zestien.new
  # p v.binaryshift 243
end
